<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");
$gender = array("2" => "Nam", "1" => "Nữ");
$arr = array(""=>"","MAT"=>"Khoa học máy tính","KDL" => "Khoa học vật liệu");
?>
<html>
    <head>
        <link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
        <title>Document</title>
    </head>

<style>
    body {
        background: #eee !important;	
    }
    .wrapper {	
        margin-top: 40px;
        margin-bottom: 80px;
    }
    .form-signin {
        
        max-width: 400px;
        height: 150px;
        padding: 15px 35px 45px;
        margin: 0 auto;
        background-color: #fff;
        border-radius: 10px/20px;
        border: 1px solid #000;
    }
    .link{
        cursor: pointer;
    }
</style>
   
<body>
<?php
session_start();
?>
<div class = 'form-signin' style="margin-top: 80px;">
    <div class = 'wrapper' style="text-align: center;">
        <div>Bạn đã đăng ký thành công</div>
        <u class="link" onclick="myFunction()">Quay lại trang danh sách sinh viên</u>
    </div>
</div>
<script>
    function myFunction() {
    location.replace("list_student.php")
    }
</script>
</body>
</html>